package model;

import type.EventType;

import java.util.HashSet;
import java.util.Set;


/**
 * Action class stores the operations for what users required in certail floor
 * Elevator managers hold collections of this object to know if they are picking
 * up people or dropping them down
 * */
public class Action {
    Set<EventType> registedActions;
    int floor;

    public Action(int floor, EventType type) {
        this.floor = floor;
        this.registedActions = new HashSet<EventType>();
        registedActions.add(type);
    }

    public void registerFloorEvent(EventType call) {
        registedActions.add(call);
    }

    public void register(EventType event) {
        registedActions.add(event);
    }

    public boolean isRequest() {
        return registedActions.contains(EventType.REQUEST);
    }
    public boolean isDispatch() {
        return registedActions.contains(EventType.DISPATCH);

    }
}
