package model;

import type.Direction;

/**
 * The bean that contains the status of an elevator
 */

public class Elevator {
    final int maxFloor;
    final int minFloor;
    final int stopTime;
    final int speed;
    final int maxWeight;
    //state
    private int currentFloor;
    private Direction directon;
    private int currentWeight;
    private Direction direction;

    public Elevator(int maxFloor, int minFloor, int stopTime, int speed, int maxWeight) {
        this.maxFloor = maxFloor;
        this.minFloor = minFloor;
        this.stopTime = stopTime;
        this.speed = speed;
        this.maxWeight = maxWeight;
    }

    public int getMaxFloor() {  return maxFloor; }
    public int getMinFloor() { return minFloor; }
    public int getStopTime() { return stopTime; }
    public int getSpeed() { return speed; }
    public int getMaxWeight() { return maxWeight; }
    public int getCurrentFloor() { return currentFloor; }
    public Direction getDirecton() { return directon; }
    public int getCurrentWeight() { return currentWeight; }

    public void setCurrentWeight(int currentWeight) {
        this.currentWeight = currentWeight;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}




