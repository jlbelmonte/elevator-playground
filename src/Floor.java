import biz.Dispatcher;
import manager.ElevatorManager;
import model.Human;
import type.Direction;

import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

/**
 * part of the building keeps track of humans desiring to go up and down, gives them a way to call elevators
 * and hold a little logic to notify them so they can go in.
 */
public class Floor {
   private final int floor;
   private Set<Human> humansUp;
    Lock upLock;
    Lock downLock;

    public Floor(int floor) {
        this.floor = floor;
    }

    Future<ElevatorManager> up;
    Future<ElevatorManager> down;


    public synchronized void call(Human human, Direction direction){
        // let's not make the dispatcher think twice don't call if one is coming
        if (direction == Direction.DOWN){
            if (down == null){
                downLock.lock();
                down = Dispatcher.getDispatcher().processCall(this.floor, direction);
                downLock.unlock();
            }
        } else {
            if (up == null){
                upLock.lock();
                up =  Dispatcher.getDispatcher().processCall(this.floor, direction);
                upLock.unlock();
            }
        }
    }

    private void notificationLoop() throws ExecutionException, InterruptedException {
        while(true){
            if (up.isDone()){
                upLock.lock();
                useFuture(up);
                upLock.unlock();
            }
            if (down.isDone()){
                downLock.lock();
                useFuture(down);
                downLock.unlock();
            }
        }

    }

    private void useFuture(Future<ElevatorManager> future) throws InterruptedException, ExecutionException {
        ElevatorManager manager = future.get();
        for (Human human : humansUp) {
            human.pingMe(manager);
        }
    }
}
