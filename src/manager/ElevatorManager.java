package manager;

import model.Action;
import model.Elevator;
import type.Direction;
import type.EventType;

import java.util.HashMap;
import java.util.Map;

/**
 * we'll have a manager per elevator
 * it holds the logic to decide if eventually will be able to pick somebody
 * also has logic for the regular functioning of the elevtaor
 * clean stuff I was doing in this floor
 * get my new weight, set direction, openDoors, playAnnoying music
 *
 */

public class ElevatorManager {
    private final int POUNDS_PER_PERSON = 150; // everybody is skinny in this building
    final Elevator elevator;
    Map<Direction, Map<Integer, Action>> nextActions;


    public void addEvent(int floor, Direction direction, EventType type) {
        Map<Integer, Action> array = nextActions.get(direction);
        Action action = array.get(floor);
        if (action == null) {
            action = new Action(floor, type);
        } else {
            action.register(type);
        }
        array.put(floor, action);
    }

    public ElevatorManager(Elevator elevator) {
        this.elevator = elevator;
        nextActions = new HashMap<Direction, Map<Integer, Action>>(Direction.values().length);

        int floors = elevator.getMaxFloor() - elevator.getMinFloor();
        for (Direction dir : Direction.values())
            nextActions.put(dir, new HashMap<Integer, Action>());
    }

    public boolean isInfloorRange(int floor) {
        return floor <= this.elevator.getMaxFloor() && floor >= elevator.getMinFloor();
    }

    private boolean inRange(int from, int to, int floor) {
        int min = (from < to) ? from : to;
        int max = (min == from) ? to : from;
        return (floor >= min && floor <= max);
    }

    public Integer arrivalTo(int floor, Direction direction) {
        int currentFloor = this.elevator.getCurrentFloor();
        Direction currentDirection = this.elevator.getDirecton();
        if (currentDirection == null) return Math.abs(floor - currentFloor) * this.elevator.getSpeed();


        boolean isCurrentDirection = direction == this.elevator.getDirecton();
        boolean isInMyPath = floor < this.elevator.getCurrentFloor() && isCurrentDirection;
        int weithDelta = 0;
        int stops = 0;
        if (isInMyPath) {
            int floors = Math.abs(floor - currentFloor);
            int time = floors * this.elevator.getSpeed();
            Map<Integer, Action> previousActions = nextActions.get(currentDirection);

            for (Integer floorKey : previousActions.keySet()) {
                if (inRange(currentFloor, floor, floorKey)) {
                    stops++;
                    Action action = previousActions.get(floorKey);

                    if (action.isRequest()) {
                        weithDelta--;
                    }
                    if (action.isDispatch()) {
                        weithDelta++;
                    }
                }
            }
            // this is guess that is wrong for sure
            // every time we stop we add or remove one person
            // you know like math this is by definition
            if (weithDelta <= 0) {
                return time + stops * this.elevator.getStopTime();
            } else {
                int plusWeight = POUNDS_PER_PERSON * weithDelta;
                int availableLoad = Math.abs(this.elevator.getCurrentWeight() + plusWeight - this.elevator.getMaxWeight());
                if (availableLoad > POUNDS_PER_PERSON) {
                    return time + stops * this.elevator.getStopTime();
                } else {
                    return Integer.MAX_VALUE;
                }
            }
        } else {
            /*
            Ok we are basically here saying  I can get you in this time if you are in my path and I think I'll be with available space.
            we can try to cover cases even if we are not in path  but this elevator goes faster because the floor range is smaller or is 20 times faster.
            As you can see in this draft I'm not covering this cases but I'm aware that they can happen according to the assumptions.
             */
            return Integer.MAX_VALUE;
        }
    }

    /* Code executed when the elevator gets to a new floor and stops*/
    private void arrived() {
        elevator.setCurrentWeight(getNewWeight());
        cleanThisFloor();
        setDirection();
    }

    private void setDirection() {
        Map<Integer, Action> dirActions = nextActions.get(this.elevator.getDirecton());
        if (dirActions.isEmpty()) {
            dirActions = nextActions.get(this.elevator.getDirecton().other());
            if (dirActions.isEmpty()) {
                // we can stop this guy until we need it
                elevator.setDirection(null);
            } else {
                elevator.setDirection(this.elevator.getDirecton().other());
            }
        }
    }

    private void cleanThisFloor() {
        for (Direction dir : Direction.values())
            nextActions.get(dir).remove(this.elevator.getCurrentFloor());
    }

    private int getNewWeight() {
        return 42;
    }

    public void goTo(int i) {
        addEvent(i, elevator.getDirecton(), EventType.REQUEST);
    }
    private void openDoors (){;;;}
    private void closeDoors (){;;;}
}
