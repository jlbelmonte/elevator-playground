package type;

public enum Direction {
    UP{
        @Override public Direction other(){
            return DOWN;
        }
    },
    DOWN{
        @Override public Direction other(){
            return DOWN;
        }
    };

    public Direction other (){
        throw new UnsupportedOperationException("each element overrides me");
    }
}
