package type;

public enum EventType {
    DISPATCH,
    REQUEST;
}
