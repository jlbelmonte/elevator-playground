package biz;

import manager.ElevatorManager;
import model.Elevator;
import type.Direction;
import type.EventType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * this class will be called from the floors to decide which of the elevators  will be assigned to the call.
 * returns the futures to the floor and the floor will decide.
 */
public class Dispatcher {
    private final List<ElevatorManager> managers;
    static Dispatcher singleton;
    private Dispatcher() {
        // configuration somehow
        int numOfElevs = 10;
        managers = new ArrayList<ElevatorManager>(numOfElevs);
        for (int i  = 0; i <numOfElevs; i++)  {
            ElevatorManager manager = new ElevatorManager(new Elevator(42,42,42,42,42 ));
            managers.add(manager);
        }

    }
    public static Dispatcher getDispatcher(){
        if (singleton == null) {
            singleton = new Dispatcher();
        }

        return singleton;
    }

    // this method is called from the calling Service will return a future for the caller only one
    // this method should really handle way more calls at the same time
    public Future<ElevatorManager> processCall(final int floor, final Direction direction){
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Callable<ElevatorManager> elevator = new Callable<ElevatorManager>(){
            @Override
            public ElevatorManager call() throws Exception {
                ElevatorManager candidate = null;
                int minTime = Integer.MAX_VALUE;
                for (ElevatorManager manager : managers) {
                    if (manager.isInfloorRange(floor)) {
                        int time = manager.arrivalTo(floor, direction);
                        if (time <= minTime) { // less or equals because we want somebody to go at least
                            minTime = time;
                            candidate = manager;
                        }
                    }
                }
                candidate.addEvent(floor, direction, EventType.DISPATCH);
                return candidate;
            }
        };
        return executor.submit(elevator);
    }
}


